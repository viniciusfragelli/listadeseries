package com.example.testmercafacil.network

import com.example.testmercafacil.ui.teste.model.vo.SerieVO
import com.google.gson.GsonBuilder
import io.reactivex.Single
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.concurrent.TimeUnit

interface WebServiceRetrofit {

    @GET("search/shows")
    fun buscaSeries(@Query("q") busca: String): Single<MutableList<SerieVO>>

}

class WebServices {
    companion object {
        val instance: WebServiceRetrofit = WebServices().initRetrofit()
    }

    private fun initRetrofit() : WebServiceRetrofit {
        val okHttpClient = OkHttpClient().newBuilder()
            .hostnameVerifier { hostname, session -> true }
            .connectTimeout(25, TimeUnit.SECONDS)
            .readTimeout(25, TimeUnit.SECONDS)
            .writeTimeout(25, TimeUnit.SECONDS)
            .build()

        return Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .baseUrl("http://api.tvmaze.com/")
            .client(okHttpClient)
            .build()
            .create(WebServiceRetrofit::class.java)
    }
}