package com.example.testmercafacil.bases

import android.arch.lifecycle.MutableLiveData
import android.databinding.BaseObservable
import android.databinding.Observable


class BaseMutableLiveData<T : BaseObservable> : MutableLiveData<T>() {

    internal var callback: Observable.OnPropertyChangedCallback =
        object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable, propertyId: Int) {
                value = value
            }
        }


    override fun setValue(value: T?) {
        super.setValue(value)
        value!!.addOnPropertyChangedCallback(callback)
    }


}