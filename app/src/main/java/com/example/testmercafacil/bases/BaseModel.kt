package com.example.testmercafacil.bases

import io.reactivex.disposables.CompositeDisposable

open class BaseModel {
    private var compositeDisposable: CompositeDisposable? = null

    protected fun getCompositeDisposable(): CompositeDisposable {
        val comp = compositeDisposable ?: CompositeDisposable()
        compositeDisposable = comp
        return comp
    }
}