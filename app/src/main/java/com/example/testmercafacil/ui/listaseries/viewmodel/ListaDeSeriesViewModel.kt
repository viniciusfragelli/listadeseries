package com.example.testmercafacil.ui.listaseries.viewmodel

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableField
import com.example.testmercafacil.bases.BaseViewModel
import com.example.testmercafacil.ui.listaseries.adapter.ItemSerie
import com.example.testmercafacil.ui.teste.model.ListaDeSeriesModel
import com.example.testmercafacil.ui.teste.model.vo.SerieVO
import org.jetbrains.anko.AlertBuilder

class ListaDeSeriesViewModel(application: Application) : BaseViewModel(application) {

    var items: MutableLiveData<MutableList<SerieVO>> = MutableLiveData<MutableList<SerieVO>>().apply { value = mutableListOf() }
    var busca: String? = null
    var model: ListaDeSeriesModel? = null
    var title: ObservableField<String>? = null

    init {
        model = ListaDeSeriesModel()
    }

    fun setup(){
        model?.buscaSeries(busca ?: "",{ result ->
            items.postValue(result)
        },{ ex ->

        })
    }

    fun onClickLista(){

    }

    private fun showError(msg: String){

    }


}