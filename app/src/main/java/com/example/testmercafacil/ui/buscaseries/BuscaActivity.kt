package com.example.testmercafacil.ui.buscaseries

import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.testmercafacil.R
import com.example.testmercafacil.databinding.ActivityBuscaBinding
import com.example.testmercafacil.ui.buscaseries.viewmodel.BuscaViewModel
import com.example.testmercafacil.ui.listaseries.ListaDeSeriesActivity
import com.example.testmercafacil.ui.teste.model.ListaDeSeriesModel

class BuscaActivity : AppCompatActivity() {


    var binding: ActivityBuscaBinding? = null
    var viewmodel: BuscaViewModel? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_busca)
        viewmodel = ViewModelProviders.of(this).get(BuscaViewModel::class.java)
        viewmodel?.context = this
        viewmodel?.callListaSeries = {
            var intent = Intent(this,ListaDeSeriesActivity::class.java)
            intent.putExtra("busca",it)
            startActivity(intent)
        }
        binding?.viewmodel = viewmodel
        binding?.setLifecycleOwner(this)
    }

}
