@file:JvmName("TesteActivityAdapter")
package com.example.testmercafacil.ui.teste.adapter

import com.example.testmercafacil.R
import android.databinding.BindingAdapter
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.testmercafacil.databinding.ItemSerieBinding
import com.example.testmercafacil.ui.listaseries.adapter.ItemSerie
import com.example.testmercafacil.ui.teste.model.vo.SerieVO
import com.squareup.picasso.Picasso

class SeriesAdapter : RecyclerView.Adapter<SeriesAdapter.ItemViewHolder>() {

    private var items: List<SerieVO> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ItemViewHolder = ItemViewHolder(parent)

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) = holder.bind(items[position])


    fun update(items: MutableList<SerieVO>) {
        this.items = items
        notifyDataSetChanged()
    }

    companion object {
        @JvmStatic
        @BindingAdapter("items")
            fun RecyclerView.bindItems(items: MutableList<SerieVO>) {
            val adapter = adapter as SeriesAdapter
            adapter.update(items)
        }
    }

    class ItemViewHolder(
        private val parent: ViewGroup,
        private val binding: ItemSerieBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_serie, parent, false)
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: SerieVO) {
            var i = ItemSerie(item.show?.name ?: "",item.show?.image?.medium ?: "")
            binding.item = i
            if(!i.img.isNullOrEmpty()) {
                Picasso.get()
                    .load(i.img)
                    .placeholder(R.drawable.ic_download_img)
                    .error(R.drawable.ic_download_error_foreground)
                    .into(binding.ivLoad);
            }else{
                binding.ivLoad.setImageResource(R.drawable.ic_download_error_foreground)
            }
        }
    }
}