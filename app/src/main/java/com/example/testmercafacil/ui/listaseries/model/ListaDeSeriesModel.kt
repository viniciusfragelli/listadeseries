package com.example.testmercafacil.ui.teste.model

import com.example.testmercafacil.bases.BaseModel
import com.example.testmercafacil.network.WebServices
import com.example.testmercafacil.ui.teste.model.vo.SerieVO
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ListaDeSeriesModel : BaseModel() {

    fun buscaSeries(busca: String, onSuccess: ((MutableList<SerieVO>) -> Unit), onError: ((Throwable) -> Unit)){
        WebServices.instance.buscaSeries(busca).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({ result ->
            onSuccess(result)
        },{ exception ->
            exception.printStackTrace()
            onError(exception)
        })
    }
}