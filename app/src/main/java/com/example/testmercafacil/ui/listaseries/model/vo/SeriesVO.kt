package com.example.testmercafacil.ui.teste.model.vo

import com.google.gson.annotations.SerializedName

data class SerieVO(
    var score: String? = null,
    var show: ShowVO? = null
)

data class ShowVO (
    var name: String? = null,
    var image: ImagemVO? = null
)

data class ImagemVO (
    var medium: String? = null
)