package com.example.testmercafacil.ui.buscaseries.viewmodel

import android.app.Application
import android.content.Context
import android.databinding.ObservableField
import android.widget.Toast
import com.example.testmercafacil.bases.BaseViewModel

class BuscaViewModel(application: Application) : BaseViewModel(application) {

    var tfSerie: ObservableField<String>? = null
    var context: Context? = null
    var callListaSeries: ((String) -> Unit)? = null

    init {
        tfSerie = ObservableField("")
    }

    fun onClickBuscar(){
        val busca = tfSerie?.get() ?: ""
        if (busca.isNullOrEmpty()){
            Toast.makeText(context,"Entre com sua serie",Toast.LENGTH_LONG);
        }
        callListaSeries?.let { it(busca) }
    }

}