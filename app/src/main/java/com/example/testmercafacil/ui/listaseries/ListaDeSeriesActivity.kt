package com.example.testmercafacil.ui.listaseries

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.example.testmercafacil.R
import com.example.testmercafacil.databinding.ActivityListaDeSeriesBinding
import com.example.testmercafacil.ui.listaseries.viewmodel.ListaDeSeriesViewModel
import com.example.testmercafacil.ui.teste.adapter.SeriesAdapter
import com.example.testmercafacil.ui.teste.model.vo.SerieVO
import kotlinx.android.synthetic.main.activity_lista_de_series.*


class ListaDeSeriesActivity : AppCompatActivity() {

    var binding: ActivityListaDeSeriesBinding? = null
    var viewmodel: ListaDeSeriesViewModel? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_lista_de_series)
        viewmodel = ViewModelProviders.of(this).get(ListaDeSeriesViewModel::class.java)
        viewmodel?.busca = intent.getStringExtra("busca") ?: ""
        binding?.viewmodel = viewmodel
        binding?.setLifecycleOwner(this)
        val layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager
        recyclerView.hasFixedSize()
        recyclerView.adapter = SeriesAdapter()
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        initObservable()
    }

    private fun initObservable(){
        viewmodel?.items?.observe(this, object : Observer<MutableList<SerieVO>> {
            override fun onChanged(t: MutableList<SerieVO>?) {
                val adapter = recyclerView.adapter as SeriesAdapter
                t ?: return
                adapter.update(t)
                adapter.notifyDataSetChanged()
            }

        })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onResume() {
        super.onResume()
        viewmodel?.setup()
    }
}
